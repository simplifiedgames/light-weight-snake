# Light-weight snake

Simple snake game written in C++ using GLFW and OpenGL. Currently in pre-alpha stage.


## Building

### 1. Installing dependencies
* **GLFW**


**Debian / Ubuntu**
```bash
$ sudo apt install libglfw3-dev 
```
**Arch / Manjaro**
>If you are on wayland:
```bash
$ sudo pacman -S glfw-wayland
```
>Or if you are using x11 session:
```bash
$ sudo pacman -S glfw-x11
```

### 2. Compiling
Use [The Meson Build System](https://mesonbuild.com/) to compile

```bash
$ CXX=clang++ CC=clang meson setup builddir --optimization s #or replace clang++ and clang with another compiler
$ cd builddir
$ ninja
```

And remove debug information it if you want (this will greatly reduce the file size)

```bash
strip snake -s -R .comment -R .gnu.version -R .note.gnu.property
```

Then run it
```bash
$ ./snake
```

## Cross-compiling for Windows **on Arch / Manjaro**

### 1. Installing dependencies
* mingw-w64-gcc
* mingw-w64-glfw
* mingw-w64-cmake
* mingw-w64-pkg-config


> You should have AUR helper ("yay" for example) or build some of them manually
```bash
$ yay -S mingw-w64-gcc mingw-w64-glfw mingw-w64-cmake mingw-w64-pkg-config
```

### 2. Compiling
```bash
$ meson setup builddir --optimization s --cross-file cross-windows
$ cd builddir
$ ninja
```

And remove debug information it if you want (this will greatly reduce the file size)

```bash
x86_64-w64-mingw32-strip snake.exe -s -R .comment -R .gnu.version -R .note.gnu.property
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[GNU General Public License v3.0](https://choosealicense.com/licenses/gpl-3.0/)
(GPL-3.0-or-later)