/*
 *Light-weight snake game
 *Copyright (C) 2021 Alekseenkov Daniil
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.

 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt.
 *
 */


#ifndef LIGHT_WEIGHT_SNAKE_SCREEN_HPP
#define LIGHT_WEIGHT_SNAKE_SCREEN_HPP

#include <glad/gl.h>

class Screen{

    public:
        //Create screen, make shader program and buffers
        Screen();
        //Set the field pointer to draw
        void setField(unsigned char *field, int width, int heigth);
        //Set grid dimensions
        void setGridDimensions(float width, float height);
        //Draw it
        void draw();
        //Change u_resolution for shader
        void setResolution(float width, float height);
        //Clean
        ~Screen();
    private:
        GLuint vbo, vao, vertexShader, fragmentShader, program, texture, u_resolution, u_dimension, u_snake;
        int fieldWidth, fieldHeight;
        unsigned char *field;
};

#endif //LIGHT_WEIGHT_SNAKE_SCREEN_HPP