/*
 *Light-weight snake game
 *Copyright (C) 2021 Alekseenkov Daniil
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.

 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt.
 *
 */

#include <main.hpp>
#include <screen.hpp>

#include <iostream>
#include <cstring>
#include <glad/gl.h>

//Initialize
int main(int argc, char **argv){

    //Declare window size, grid size, snake lenght, max food on field and snake step time variables
    int windowWidth = 800, windowHeight = 800, gridWidth = 25, gridHeight = 25, defaultSnakeLenght = 5, maxFood = 3, stepTime = 100;

    //Parsing arguments from command line
    try{
        for (int i = 1; i < argc; i++){
            char* argument = argv[i];
            bool argumentWasntUsed = false;
            if (i < argc - 1) {
                char* nextArgument = argv[i + 1];

                //That is handler for arguments WITH a parameter
                if (!std::strcmp(argument, "--window-width")){
                    windowWidth = std::stoi(nextArgument);
                }
                else if (!std::strcmp(argument, "--window-height")){
                    windowHeight = std::stoi(nextArgument);
                }
                else if (!std::strcmp(argument, "--grid-width")){
                    gridWidth = std::stoi(nextArgument);
                }
                else if (!std::strcmp(argument, "--grid-height")){
                    gridHeight = std::stoi(nextArgument);
                }
                else if (!std::strcmp(argument, "--snake-length")){
                    defaultSnakeLenght = std::stoi(nextArgument);
                }
                else if (!std::strcmp(argument, "--max-food")){
                    maxFood = std::stoi(nextArgument);
                }
                else if (!std::strcmp(argument, "--step-time")){
                    stepTime = std::stoi(nextArgument);
                }
                else{
                    argumentWasntUsed = true;
                }
                i++;
            }
            else{
                argumentWasntUsed = true;
            }

            //That is handler for arguments with NO parameter
            if (!std::strcmp(argument, "--help")){
                printUsage();
                return 0;
            }
            else if (argumentWasntUsed){
                throw std::invalid_argument("Bad usage");
            }
        }
    }


    catch (std::invalid_argument const&) {
        //Print usage
        printUsage();
        return 1;
    }

    //Set error callback
    glfwSetErrorCallback(errorCallback);

    //Init glfw
    if (!glfwInit()){
        std::cerr << "Error: GLFW failed to init, exiting..." << std::endl;
        return 1;
    }


    //Create window
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    GLFWwindow* window = glfwCreateWindow(windowWidth, windowHeight, "Snake", NULL, NULL);
    if (!window){
        std::cerr << "Error: GLFW failed to create window, exiting..." << std::endl;
        glfwTerminate();
        return 1;
    }

    //Set window callbacks
    glfwSetKeyCallback(window, keyCallback);

    //Make OpenGL context current
    glfwMakeContextCurrent(window);

    //Initialize GLAD
    if (!gladLoadGL(glfwGetProcAddress)){
        std::cerr << "Error: GLAD failed to init, exiting..." << std::endl;
        return 1;
    }

    handleGame(window, windowWidth, windowHeight, gridWidth, gridHeight, defaultSnakeLenght, maxFood, stepTime);

    //Clean
    glfwDestroyWindow(window);
    glfwTerminate();
    delete gameManager;

}

//Start game
void handleGame(GLFWwindow *window, int windowWidth, int windowHeight, int gridWidth, int gridHeight, int defaultSnakeLenght, int maxFood, int stepTime){
    //Create screen object
    gameManager = new GameManager(gridWidth, gridHeight, defaultSnakeLenght, maxFood);
    Screen screen = Screen();
    screen.setField(gameManager->getField(), gridWidth, gridHeight);
    screen.setGridDimensions(gridWidth, gridHeight);

    //Main loop
    while (!glfwWindowShouldClose(window))
    {
        //Poll events
        glfwPollEvents();

        //Clear screen
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Get framebuffer size
        int fbWidth, fbHeight;
        glfwGetFramebufferSize(window, &fbWidth, &fbHeight);
        glViewport(0, 0, fbWidth, fbHeight);

        //Draw here
        screen.setResolution(fbWidth, fbHeight);
        gameManager->loop(stepTime);
        screen.draw();


        //Swap buffers
        glfwSwapBuffers(window);

    }
}

//Print usage
void printUsage(){
    std::cout << "Usage!\nOptions:"
    "\n\t--window-width <int>"
    "\n\t--window-height <int>"
    "\n\t--grid-width <int>"
    "\n\t--grid-height <int>"
    "\n\t--snake-length <int>"
    "\n\t--max-food <int>"
    "\n\t--step-time <int>"
    "\n\t--help" << std::endl;
}

//Callback functions

//Error callback
void errorCallback(int error, const char* description){
    fprintf(stderr, "Error: %s\n", description);
}

//Key press callback
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods){
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    else if (key == GLFW_KEY_UP && action == GLFW_PRESS){
        gameManager->setDirection(GameManager::eDirection::Up);
    }
    else if (key == GLFW_KEY_DOWN && action == GLFW_PRESS){
        gameManager->setDirection(GameManager::eDirection::Down);
    }
    else if (key == GLFW_KEY_LEFT && action == GLFW_PRESS){
        gameManager->setDirection(GameManager::eDirection::Left);
    }
    else if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS){
        gameManager->setDirection(GameManager::eDirection::Right);
    }
}
