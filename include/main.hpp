/*
 * Light-weight snake game
 *Copyright (C) 2021 Alekseenkov Daniil
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.

 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt.
 *
 */

#ifndef LIGHT_WEIGHT_SNAKE_MAIN_HPP
#define LIGHT_WEIGHT_SNAKE_MAIN_HPP

#define GLFW_INCLUDE_NONE

#include <game.hpp>
#include <GLFW/glfw3.h>


//Initialize
int main(int argc, char **argv);

//Start game
void handleGame(GLFWwindow *window, int windowWidth, int windowHeight, int gridWidth, int gridHeight, int defaultSnakeLenght, int maxFood, int stepTime);

//Print usage
void printUsage();

//Callback functions

//Error callback
void errorCallback(int error, const char* description);

//Key press callback
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);

//Game manager instance
GameManager *gameManager;
#endif //LIGHT_WEIGHT_SNAKE_MAIN_HPP