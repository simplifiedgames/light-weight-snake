/*
 *Light-weight snake game
 *Copyright (C) 2021 Alekseenkov Daniil
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.

 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt.
 *
 */

#include <game.hpp>
#include <chrono>
#include <iostream>

//Create game field and initialize. Field is RGB data. R is snake and tail, G is food. B is not set right now
GameManager::GameManager(int width, int height, int defaultSnakeLength, int maxFoodInTheField){
    //Create random device
    rng = std::mt19937(randomDevice());

    //Set values
    fieldWidth = width;
    fieldHeight = height;
    this->defaultSnakeLength = defaultSnakeLength;
    maxFood = maxFoodInTheField;

    field = new unsigned char[width * height * 3];

    //Initialize array and sets variables to zero
    restart();
}

//Loop function, must be often called
void GameManager::loop(int period){
    int64_t currentTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    if (currentTime - lastLoopTime > period){

        //Decrease snake tail values
        for (int i = 0; i < fieldWidth * fieldHeight * 3; i+=3)
        {
            if (field[i] > 0) field[i]--;
        }

        //Remove old direction from queue if it is not the only one
        if (inputQueue.size() > 1){
            inputQueue.pop();
        }

        eDirection direction = inputQueue.front();

        //Change head position
        switch (direction)
        {
        case eDirection::Up:
            headY++;
            break;
        case eDirection::Down:
            headY--;
            break;
        case eDirection::Left:
            headX--;
            break;
        case eDirection::Right:
            headX++;
            break;
        }

        //Force the value to stay within the field
        headY = headY % fieldHeight;
        headX = headX % fieldWidth;
        
        if (headY < 0){headY = fieldHeight + headY;}
        if (headX < 0){headX = fieldWidth + headX;}

        //Restart if the snake hits its tail
        if (field[(headX + headY * fieldWidth) * 3] != 0){
            restart();
            return;
        }

        //Summon new food, increase snake length and delete existing one if it was eaten
        if (field[(headX + headY * fieldWidth) * 3 + 1] != 0){
            putFood();
            field[(headX + headY * fieldWidth) * 3 + 1] = 0;
            snakeLength++;
        }

        //Set tail length to the snake's head position
        field[(headX + headY * fieldWidth) * 3] = snakeLength;

        //Set the time of the last loop equal to the current time 
        lastLoopTime = currentTime;
    }
}

//Put food on the field to the random location
void GameManager::putFood(){
    //Check if there is anything else in random location on the field, then place the food in that position and try again if there is something
    int maxIter = fieldHeight * fieldWidth * 64;
    int currentIter = 0;
    while (currentIter < maxIter){
        int snakeLocation = (randomInt(0, fieldWidth) + randomInt(0, fieldHeight) * fieldWidth) * 3;
        int foodLocation = snakeLocation + 1;
        if (field[snakeLocation] == 0 && field[foodLocation] == 0){
            field[foodLocation] = 255;
            break;
        }
        currentIter++;
    }
}


//Start/Restart the game, initialize field and set variables to zero
void GameManager::restart(){
    //Initialize field
    for (int i = 0; i < fieldWidth * fieldHeight * 3; i++){
        field[i] = 0;
    }

    //Set default values
    lastLoopTime = 0;
    headX = 0;
    headY = 0;
    snakeLength = defaultSnakeLength;
    while (!inputQueue.empty())
    {
        inputQueue.pop();
    }
    inputQueue.push(eDirection::Up);
    

    //Put first food
    for (int i = 0; i < maxFood; i++){
        putFood();
    }
}

void GameManager::setDirection(eDirection direction){
    //Prevent the snake's head from moving inward
    if (!( 
        (inputQueue.back() == eDirection::Down && direction == eDirection::Up) ||
        (inputQueue.back() == eDirection::Up && direction == eDirection::Down) ||
        (inputQueue.back() == eDirection::Left && direction == eDirection::Right) ||
        (inputQueue.back() == eDirection::Right && direction == eDirection::Left)
        ) && inputQueue.size() < 4 && inputQueue.back() != direction){
            inputQueue.push(direction);
        }
}

//Generate random number between min and max - 1
int GameManager::randomInt(int min, int max){
    std::uniform_int_distribution<std::mt19937::result_type> ran(min, max - 1);
    return ran(rng);
}

//Get current game field
unsigned char *GameManager::getField(){
    return field;
}

//Cleanup
GameManager::~GameManager(){
    delete[] field;
}