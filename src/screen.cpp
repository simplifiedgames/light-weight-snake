/*
 *Light-weight snake game
 *Copyright (C) 2021 Alekseenkov Daniil
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.

 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt.
 *
 */

#include <screen.hpp>
#include <algorithm>

//Vertices of the screen
const GLfloat vertices[] = {
    -1, 1, 0,
    1, 1, 0,
    -1, -1, 0,
    1, -1, 0,

};

//This shows X if no field set
const GLubyte defaultSnakeData[] = {
    255, 0, 0, 255,
    0, 255, 255, 0,
    0, 255, 255, 0,
    255, 0, 0, 255,
};

//Vertex shader
const GLchar *const vertex_text =
"#version 330 core\n"
"in vec3 vp;"
"void main()"
"{"
    "gl_Position = vec4(vp, 1.0);"
"}";


//Fragment shader, draws grid and snake
const GLchar *const fragment_text =
"#version 330 core\n"
"uniform vec2 u_resolution,u_dimension;"
"uniform sampler2D u_snake;"
"out vec4 FragColor;"
"vec4 get_color_of_grid(vec2 vt)"
"{"
    "if(int(mod(gl_FragCoord.x,vt.x))==0||int(mod(gl_FragCoord.y,vt.y))==0)"
        "return vec4(0.,0.,0.,.5);"
    "else"
        " return vec4(1.,1.,1.,1.);"
"}"
"vec4 get_color_of_inner(vec2 vt)"
"{"
    "vec4 result=texture2D(u_snake,gl_FragCoord.xy/u_resolution);"
    "if(result.x>0)"
        "return vec4(result.x*30.,.0,.0,1.);"
    "else if(result.y>0)"
        "return vec4(.5,result.y,.3,1.);"
    "return vec4(1.,1.,1.,1.);"
"}"
"void main()"
"{"
    "vec2 vt=u_resolution/u_dimension;"
    "vec4 color=get_color_of_grid(vt)*get_color_of_inner(vt);"
    "FragColor=color;"
"}";






//Create screen, make shader program and buffers
Screen::Screen(){
    //Create vertex buffer
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    //Create vertex array object
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    //Compile shaders
    vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertex_text, NULL);
    glCompileShader(vertexShader);
 
    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragment_text, NULL);
    glCompileShader(fragmentShader);

    //Create program
    program = glCreateProgram();
    glAttachShader(program, fragmentShader);
    glAttachShader(program, vertexShader);
    glLinkProgram(program);

    //Find uniform address  
    u_resolution = glGetUniformLocation(program, "u_resolution");
    u_dimension = glGetUniformLocation(program, "u_dimension");
    u_snake = glGetUniformLocation(program, "u_snake");

    //Creating default texture
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 4, 4, 0, GL_RED, GL_UNSIGNED_BYTE, defaultSnakeData);
    glGenerateMipmap(GL_TEXTURE_2D);

    //Set uniform defaults
    glUseProgram(program);
    glUniform2f(u_resolution, 100, 100);
    glUniform2f(u_dimension, 16, 16);
}

//Set the field pointer to draw
void Screen::setField(unsigned char *field, int width, int height){
    this->field = field;
    fieldWidth = width;
    fieldHeight = height;
}

//Set grid dimensions
void Screen::setGridDimensions(float width, float height){
    glUniform2f(u_dimension, width, height);
}

//Draw it
void Screen::draw(){
    glUseProgram(program);
    glBindVertexArray(vao);

    if (field != nullptr){
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, fieldWidth, fieldHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, field);
        glGenerateMipmap(GL_TEXTURE_2D);
    }

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

//Change u_resolution for shader
void Screen::setResolution(float width, float height){
    glUseProgram(program);
    glUniform2f(u_resolution, width, height);
}

//Clean
Screen::~Screen(){
    glDeleteTextures(1, &texture);
    glDeleteProgram(program);
    glDeleteShader(fragmentShader);
    glDeleteShader(vertexShader);
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &vbo);
}