/*
 *Light-weight snake game
 *Copyright (C) 2021 Alekseenkov Daniil
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.

 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt.
 *
 */

#ifndef LIGHT_WEIGHT_SNAKE_GAME_HPP
#define LIGHT_WEIGHT_SNAKE_GAME_HPP

#include <queue>
#include <random>

//Manage the snake game
class GameManager{
    public:

        enum eDirection{
            Up,
            Down,
            Left,
            Right,
        };

        //Create game field and initialize. Field is RGB data. R is snake and tail, G is food. B is not set right now
        GameManager(int width, int height, int defaultSnakeLength, int maxFoodInTheField);
        //Loop function, must be often called
        void loop(int period = 500);
        //Put food on the field to the random location
        void putFood();
        //Start/Restart the game, initialize field and set variables to zero
        void restart();
        //Set direction of snake head
        void setDirection(eDirection direction);
        //Generates random number between min and max - 1
        int randomInt(int min, int max);
        //Get current game field
        unsigned char *getField();
        //Cleanup
        ~GameManager();
    private:
        unsigned char *field;
        int fieldWidth, fieldHeight, headX, headY, snakeLength, defaultSnakeLength, maxFood;
        int64_t lastLoopTime;
        std::queue<eDirection> inputQueue;

        //Random device
        std::random_device randomDevice;
        std::mt19937 rng;
};

#endif //LIGHT_WEIGHT_SNAKE_GAME_HPP